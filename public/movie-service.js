const movieService = {
    getDefaultMovies: () => fetch('/movies').then(res => res.json()).catch(e => console.log(e)),
    postMovie: (data) => fetch('/movie', { method: 'POST', headers: {
        'Content-Type': 'application/json'
    }, body: JSON.stringify(data)}),
    updateMovie: (data) => fetch(`/movie/${data._id}`, { method: 'PUT', headers: {
        'Content-Type': 'application/json'
    }, body: JSON.stringify(data)}),
    deleteMovie: (id) => fetch(`/movie/${id}`, { method: 'DELETE', headers: {
        'Content-Type': 'application/json'
    }})
}