class MovieElementComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            showEditForm: false,
            movie: this.props.movie
        }
    }

    handleSubmit(e) {
        e.preventDefault();
        this.setState({
            movie: {
                _id: this.props.movie._id,
                title: this.editForm.title.value,
                year: this.editForm.year.value,
                genre: this.editForm.genre.value,
                rate: this.editForm.rate.value,
                desc: this.editForm.desc.value
            }
        }, () => movieService.updateMovie(this.state.movie));
    }

    render() {
        return (
            <div className="movie-element">
                <div onClick={() => this.setState({ showEditForm: !this.state.showEditForm })}>
                    Елемент списку фільмів
                    <div>Назва: {this.props.movie.title}</div>
                    <div>Рік випуску: {this.props.movie.year}</div>
                    <div>Жанр: {this.props.movie.genre}</div>
                    <div>Рейтинг: {this.props.movie.rate}</div>
                    <div>Опис: {this.props.movie.desc}</div>
                </div>
                <button onClick={() => this.props.deleteMovie(this.props.movie._id)}>Видалити</button>  
        
                {this.state.showEditForm ? <form id="movie-edit" name="movie" ref={el => this.editForm = el} onSubmit={e => this.handleSubmit(e)}>
                    <input type="text" name="title" defaultValue={this.state.movie.title} />
                    <input type="text" name="year" defaultValue={this.state.movie.year} />
                    <input type="text" name="genre" defaultValue={this.state.movie.genre} />
                    <input type="text" name="rate" defaultValue={this.state.movie.rate}/>
                    <textarea name="desc" defaultValue={this.state.movie.desc}></textarea>
                    <button>Оновити</button>
                </form> : null}
            </div>
        )
    }
};


