class MovieAdd extends React.Component {
    constructor() {
        super();
        this.state = {
            showMovieAdd: false,
            jsonError: false
        };
    }

    submitMovies() {
        try {
            let hopeValidJson = null;
            this.setState({ jsonError: false });
            if (this.jsonMovies.value) {
                hopeValidJson = JSON.parse(this.jsonMovies.value.toString());
                movieService.postMovie(hopeValidJson);
            } else {
                movieService.postMovie({ title: this.form.title.value, year: this.form.year.value, genre: this.form.genre.value, rate: this.form.rate.value, desc: this.form.desc.value });
            }
        } catch(e) {
            this.setState({ jsonError: e });
        }
    }
    render() {
        return (
            <div>
                <button onClick={() => this.setState({ showMovieAdd: !this.state.showMovieAdd })}>Форма додавання</button>
                {this.state.showMovieAdd ? <div><form id="movie-add" name="movie-add" ref={fields => this.form = fields}>
                    <label htmlFor="title">Заголовок</label><input type="text" name="title" />
                    <label htmlFor="year">рік</label><input type="text" name="year" />
                    <label htmlFor="genre">жанр</label><input type="text" name="genre" />
                    <label htmlFor="rate">рейтинг</label><input type="text" name="rate" />
                    <label htmlFor="desc">опис</label><textarea name="desc"></textarea>
                </form>
                <div>або</div>
                <label htmlFor="bulk">Додати багато фільмів одразу у json</label>
                <textarea name="bulk" ref={data => this.jsonMovies = data}></textarea>
                {this.state.jsonError ? <div>Будь ласка перевірте валідність введеного JSON {this.state.jsonError}</div> : null}
                <button  onClick={() => this.submitMovies()}>Відправити</button></div> : null}
            </div>
        );
    }
}