const MoviesListComponent = (props) => (
    <div>
        <div>Список фільмів</div>
        {props.movies.map(movie => <MovieElementComponent movie={movie} key={movie._id} deleteMovie={props.deleteMovie} />)}
    </div>
);
