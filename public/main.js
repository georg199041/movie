class MainComponent extends React.Component {
  constructor() {
    super();
    this.state = {
      movies: []
    }
  }

  componentDidMount() {
    movieService.getDefaultMovies().then(data => this.setState({ movies: data }));
  }

  deleteMovie(id) {
    this.setState({
      movies: this.state.movies.filter(item => item._id !== id)
    });
    movieService.deleteMovie(id);
  }

  render() {
    return (
      <div>
        <div>Довідник по кінофільмах</div>
        <MovieAdd />
        <MoviesListComponent movies={this.state.movies} deleteMovie={this.deleteMovie.bind(this)} /> 
      </div>
      
    )
  }
}