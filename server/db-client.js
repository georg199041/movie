const MongoDB = require('mongodb');
const MongoClient = MongoDB.MongoClient;
const ObjectID = MongoDB.ObjectID;
let dbConnect = null;
let db = null;
let collection = null;

module.exports = () => {
    const url = 'mongodb://localhost:27017';
    const service = {
        connect: async() => {
            dbConnect = await MongoClient.connect(url, { useNewUrlParser: true, useUnifiedTopology: true });
            db = dbConnect.db('kursak');
            collection = db.collection('movies');
            return dbConnect;
        },
        disconnect: () => {
            dbConnect.close();
        },
        create: async(data) => {
            if (Array.isArray(data)) {
                return await collection.insertMany(data).catch(err => console.log(err));
            } else {
                return collection.insert(data).catch(err => console.log(err));
            }
        },
        read: async(id = null) => {
            return await collection.find(id ? { _id: ObjectID(id) } : {}).toArray().catch(err => console.log(err));
        },
        update: async(id, data) => {
            delete data._id;
            return await collection.updateOne({ _id: ObjectID(id) }, { $set: data }).catch(err => console.log(err));
        },
        delete: async(id) => {
            return await collection.deleteOne({ _id: ObjectID(id) }).catch(err => console.log(err));
        }
    };

    return service;
}